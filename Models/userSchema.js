const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
			firstName : {
				type : String,
				required : [true, "First name is required"]
			},
			lastName : {
				type : String,
				required : [true, "Last name is required"]
			},
			email : {
				type : String,
				required : [true, "Email is required"]
			},
			password : {
				type : String,
				required : [true, "Password is required"]
			},
			isAdmin : {
				type : Boolean,
				default : false
			},
			mobileNo : {
				type : String, 
				required : [true, "Mobile No is required"]
			},

			orders:[{
				products: [{
					productId: {
						type: String,
						required: [true, "UserId is required!"]
					},
					quantity:{
						type: Number,
						required: [true, "UserId is required!"]
					}

				}],
				totalAmount: {
					type: Number,
					default: 0
					},
				OrderedOn: {
					type: Date,
					default: new Date()
					}
				}]
		})

module.exports = mongoose.model("User", userSchema);
