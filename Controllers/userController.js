const mongoose = require("mongoose");
const User = require("../Models/userSchema.js");
const Product = require("../Models/productSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

//USER REGISTRATION
module.exports.userRegistration = (request, response) => {

		const input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return response.send(false)
			}else{
				let newUser = new User({
					firstName: input.firstName,
					lastName: input.lastName,
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
					mobileNo: input.mobileNo
				})
				newUser.save()
				.then(save => {
					return response.send(true)
				})
				.catch(error => {
					return response.send(error)
				})
			}
		})
		.catch(error => {
			return response.send(error)
		})
}

//USER AUTHENTICATION
module.exports.userAuthentication = (request, response) => {
		let input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result === null){
				return response.send(false)
			}else{
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return response.send({auth: auth.createAccessToken(result)});
				}else{
					return response.send(false)
				}
			}
		})
		.catch(error => {
			return response.send(error);
		})

}

//RETRIEVE USER DETAIL
module.exports.getProfile = (request, response) => {
	// let input = request.body;
	const userData = auth.decode(request.headers.authorization);
	return User.findById(userData._id).then(result =>{
		result.password = "";

		return response.send(result);
	})

}

//MODULE FOR CREATE ORDER
module.exports.orderProduct = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;
	let input = request.body;
	let price = await Product.findById(productId).then(result => result.price).catch(error => error);

	let isUserUpdated = await User.findById(userData._id)
	.then(result => {
		if(result === null){
			
			return false
		}
		else{
			if(userData.isAdmin){
				
				return response.send(false);
			}else{
			
				result.orders.push(
					{
						productId: productId,
						orders:[
						{
							quantity:input.quantity
						}],
						totalAmount:input.quantity*price

					});

				return result.save()
				.then(save => true)
				.catch(error => false)
			}
			
		}
	})

	let isProductUpdated = await Product.findById(productId).then(result => {
		console.log(result);
		if(result === null){
			return false;
		}else{
			result.order.push({userId: userData._id});

			return result.save()
			.then(save => true)
			.catch(error => error);
		}
	})

	
	console.log(isUserUpdated);
	console.log(isProductUpdated);


	if(isUserUpdated && isProductUpdated){
		return response.send(true);
	}else{
		return response.send(false);
	}

};
