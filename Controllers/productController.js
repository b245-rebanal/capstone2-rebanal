const Product = require("../Models/productSchema.js");
const auth = require("../auth.js")


// MODULE FOR CREATING PRODUCT
module.exports.addProduct = (request, response) =>{
	let input = request.body;
	let userData =auth.decode(request.headers.authorization);

	let newProduct = new Product({
		name: input.name,
		description: input.description,
		price: input.price
	});

	if(userData.isAdmin){
		Product.findOne({name: input.name})
		.then(result => {
			if(result !== null){
				return response.send(false)
			}else{
				return newProduct.save()
						.then(product =>{
							response.send(product);
						})
						.catch(error =>{
							console.log(error);
							response.send(false);
						})
			}
		})
		.catch(error => {
			return response.send(error)
		})

	}else{
		return response.send("You are not an admin!")
	}
	
}

//MODULE FOR RETRIEVING ALL PRODUCTS
module.exports.allProducts = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	

	if(!userData.isAdmin){
		return response.send(false);
	}else{
		Product.find({})
		.then(result=> response.send(result))
		.catch(error => response.send(error));
		console.log(userData);
	}
}


//MODULE FOR RETRIEVING ALL ACTIVE PRODUCTS
module.exports.allAvailableProducts = (request, response) => {

	Product.find({isAvailable: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

//MODULE FOR RETRIEVING SINGLE PRODUCT
module.exports.productDetails = (request, response) => {
	//to get the params from the url
	const productId = request.params.productId;

	Product.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

//MODULE FOR UPDATING PRODUCT
module.exports.updateProduct = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	const input = request.body;


	if(!userData.isAdmin){
		return response.send(false)
	}else{
	
	await Product.findOne({_id: productId})
		.then(result =>{
			if(result === null){
				return response.send(false)
			}else{
				let updatedProduct= {
					name: input.name,
					description: input.description,
					price: input.price
				}
				
				Product.findByIdAndUpdate(productId, updatedProduct, {new: true})
				.then(result => {
					console.log(result);
					return response.send(result)})
				.catch(error => response.send(error));
			}
		})
		
	}
}


//MODULE FOR ARCHIVING PRODUCT
module.exports.archiveProduct = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send(false);
	}else{
		Product.findById(productId)
		.then(result => {
			if(result === null){
				return response.send(false)
			}else{
				let productArchive = {
					isAvailable: input.isAvailable
				}


				Product.findByIdAndUpdate(productId, productArchive, {new: true})
				.then(result => {
					return response.send(true)
				})
				.catch(error => {
					return response.send(error)
				})
			}
		})
		.catch(error => response.send(error));
	}

}