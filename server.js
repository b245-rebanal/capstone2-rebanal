const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./Routes/userRoute.js");
const productRoutes = require("./Routes/productRoute.js");



const port = 3001;
const app = express();
	mongoose.set('strictQuery', true);
	mongoose.connect("mongodb+srv://admin:admin@batch245-rebanal.hxprcml.mongodb.net/Capstone_2_rebanal?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	})


	let db = mongoose.connection;

	//For error handling
	db.on("error", console.error.bind(console, "Connection Error!"));

	//For validation of the connection
	db.once("open", () => {console.log('We are connected to the cloud!')});




// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());

//routing
app.use("/user", userRoutes);
app.use("/product", productRoutes);




app.listen(port, ()=> console.log(`Server is running at Port ${port}`))