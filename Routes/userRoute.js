const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../Controllers/userController.js")

// [ROUTES]

// ROUTE FOR USER REGISTRATION
router.post("/register", userController.userRegistration);

// ROUTE FOR USER AUTHENTICATION
router.post("/login", userController.userAuthentication);

// ROUTE FOR USER DETAILS
router.get("/details", auth.verify, userController.getProfile);

// ROUTE FOR CREATE ORDER
router.post("/order/:productId", auth.verify, userController.orderProduct);


module.exports = router;