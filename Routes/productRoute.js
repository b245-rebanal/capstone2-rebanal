const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productController = require("../Controllers/productController");


// ROUTE FOR CREATING PRODUCT
router.post("/", auth.verify, productController.addProduct);

//ROUTE FOR RETREIVING ALL PRODUCTS
router.get("/all", auth.verify, productController.allProducts);

//ROUTE FOR RETREIVING ALL ACTIVE PRODUCTS
router.get("/allAvailable", productController.allAvailableProducts);

//ROUTE FOR RETRIEVING SINGLE PRODUCT
router.get("/:productId", productController.productDetails);

//ROUTE FOR UPDATING PRODUCT
router.put("/update/:productId", auth.verify, productController.updateProduct);

//ROUTE FOR ARCHIVING PRODUCT
router.put("/toArchive/:productId", auth.verify, productController.archiveProduct);

module.exports = router;